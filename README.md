# Cloud Restriction Solver for Google App Engine (CRS4GAE)

Cloud Restriction Solver for Google App Engine (CRS4GAE), an instantiation of the CRS approach designed to help the migration of applications to the Google’s
Pass environment.

There are customizations of engines *CRSAnalyzer* and *CRSRefactor* in relation to GAE constraints: writing files and threads.

CRS4GAE is based on AutoRefactor, described below.

## AutoRefactor

The AutoRefactor project delivers free software that automatically refactor code bases.

The aim is to fix language/API usage in order to deliver smaller, more maintainable and more expressive code bases.

This is an Eclipse plugin to automatically refactor Java code bases.

You will find much more information on [http://autorefactor.org](http://autorefactor.org): goals, features, usage, samples, installation, links.

### License

AutoRefactor is distributed under the terms of both the
Eclipse Public License v1.0 and the GNU GPLv3+.

See LICENSE-ECLIPSE, LICENSE-GNUGPL, and COPYRIGHT for details.