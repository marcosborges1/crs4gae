package org.autorefactor.analyzer;



import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class CRSConsole {

	private MessageConsole myConsole;
	private MessageConsoleStream out;

	public CRSConsole() {
		this.myConsole = findConsole("CRSAnalyser");
		//myConsole.setFont(new Font(Display.getDefault(), "Verdana", 12, SWT.NORMAL));
		this.out = myConsole.newMessageStream();
	}

	private MessageConsole findConsole(String name) {
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = plugin.getConsoleManager();
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++)
			if (name.equals(existing[i].getName()))
				return (MessageConsole) existing[i];
		// no console found, so create a new one
		MessageConsole myConsole = new MessageConsole(name, null);
		conMan.addConsoles(new IConsole[] { myConsole });
		return myConsole;
	}
	
	public void clear() {
		this.myConsole.clearConsole();
	}

	public void println(String message) {
		out.println(message);
	}

}