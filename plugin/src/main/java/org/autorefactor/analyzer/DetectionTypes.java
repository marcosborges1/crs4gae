package org.autorefactor.analyzer;

public enum DetectionTypes {
	instanciation, variableDeclaration, methodDeclaration, extension, implementation
}
