package org.autorefactor.analyzer;

import java.util.HashMap;
import java.util.List;

public class GAE extends PaaS {

	public GAE() {
		this.abbreviation = "GAE";
		this.description = "Google App Engine";
		listRestrictedClasses = new HashMap<String, List<DetectionTypes>>();
	}

	public void registerRestritedClasses() {
		listRestrictedClasses.put("java.io.FileWriter", addDetections(DetectionTypes.variableDeclaration,
				DetectionTypes.methodDeclaration, DetectionTypes.extension, DetectionTypes.instanciation));
		listRestrictedClasses.put("java.io.FileOutputStream", addDetections(DetectionTypes.variableDeclaration,
				DetectionTypes.methodDeclaration, DetectionTypes.extension, DetectionTypes.instanciation));
		listRestrictedClasses.put("java.io.File", addDetections(DetectionTypes.variableDeclaration,
				DetectionTypes.methodDeclaration, DetectionTypes.extension, DetectionTypes.instanciation));
		listRestrictedClasses.put("java.lang.Thread", addDetections(DetectionTypes.instanciation));
	}

}
