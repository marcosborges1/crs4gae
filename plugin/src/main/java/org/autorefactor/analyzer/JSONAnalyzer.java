package org.autorefactor.analyzer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class JSONAnalyzer extends JSONObject {

	public JSONArray getValuesOfArray(JSONObject obj, String... identifiers) {
		JSONObject jsonAux = obj, jsonAux2 = null;
		for (int i = 0; i <= identifiers.length - 2; i++) {
			jsonAux2 = (JSONObject) jsonAux.get(identifiers[i]);
			jsonAux = jsonAux2;
			if(jsonAux2==null) {return null;}
		}
		JSONArray jsonArray = (JSONArray) jsonAux2.get(identifiers[identifiers.length - 1]);
		return jsonArray;
	}

	public JSONObject getObjects(JSONObject obj, String... identifiers) {
		JSONObject jsonAux = obj, jsonAux2 = null;
		for (int i = 0; i <= identifiers.length - 1; i++) {
			jsonAux2 = (JSONObject) jsonAux.get(identifiers[i]);
			jsonAux = jsonAux2;
		}
		return jsonAux2;
	}

	public JSONObject[] createObjects(String... identifiers) {
		JSONObject[] objs = new JSONObject[identifiers.length];
		for (int i = identifiers.length - 1; i >= 0; i--) {
			objs[i] = new JSONObject();
			objs[i].put(identifiers[i], (i == identifiers.length - 1) ? new JSONObject() : objs[i + 1]);
		}
		return objs;
	}

	public JSONObject[] createObjects(JSONObject obj, String... identifiers) {
		JSONObject[] objs = new JSONObject[identifiers.length];

		for (int i = identifiers.length - 1; i >= 0; i--) {
			objs[i] = (i == 0) ? obj : new JSONObject();
			objs[i].put(identifiers[i], (i == identifiers.length - 1) ? new JSONObject() : objs[i + 1]);
		}
		return objs;
	}

	public void JSONCreateArrayInObject(JSONObject obj, String qualifiedName, String typeValue) {

		JSONArray arrayVariable = new JSONArray();
		arrayVariable.add(typeValue);
		obj.put(qualifiedName, arrayVariable);
	}

	public String[] jsonArrayToString(JSONArray jsonArray) {
		String[] arrayString = new String[jsonArray.size()];
		for (Object object : jsonArray) {
			arrayString[jsonArray.indexOf(object)] = (String) object;
		}
		return arrayString;
	}

	public void addElementInArray(JSONArray jsonArray, Object object) {
		if (!jsonArray.contains(object)) {
			jsonArray.add(object);
		}
	}

	public String getNameObject(JSONObject obj) {
		String[] arrayString = obj.toString().split(":");
		String name = arrayString[0].replace("{", "").replace("}", "").replace("\"", "");
		return name;
	}

	public JSONObject getSelf(JSONObject obj) {
		return (JSONObject) obj.get(getNameObject(obj));
	}
	//Add after
	public JSONObject getObjects(String... identifiers) {
		JSONObject jsonAux = this, jsonAux2 = null;
		
//		jsonAux2 = (JSONObject) jsonAux.get(identifiers[0]);
//		System.out.println(jsonAux2);
		
		for (int i = 0; i <= identifiers.length - 1; i++) {
			if(jsonAux.get(identifiers[i])!=null) {
				jsonAux2 = (JSONObject) jsonAux.get(identifiers[i]);
				jsonAux = jsonAux2;
				System.err.println(jsonAux2);
			} else {
				return null;
			}
		}
		
		return jsonAux2;
	}
}
