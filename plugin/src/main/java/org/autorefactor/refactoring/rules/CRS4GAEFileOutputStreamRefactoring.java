/* 
 * AutoRefactor - Eclipse plugin to automatically refactor Java code bases. 
 * 
 * Copyright (C) 2013-2015 Jean-Noël Rouvignac - initial API and implementation 
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program under LICENSE-GNUGPL.  If not, see 
 * <http://www.gnu.org/licenses/>. 
 * 
 * 
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution under LICENSE-ECLIPSE, and is 
 * available at http://www.eclipse.org/legal/epl-v10.html 
 */
package org.autorefactor.refactoring.rules;

import static org.autorefactor.refactoring.ASTHelper.VISIT_SUBTREE;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.autorefactor.analyzer.JSONAnalyzer;
import org.autorefactor.refactoring.ASTBuilder;
import org.autorefactor.refactoring.ASTHelper;
import org.autorefactor.refactoring.Refactorings;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/** See {@link #getDescription()} method. */
@SuppressWarnings("javadoc")
public class CRS4GAEFileOutputStreamRefactoring extends AbstractRefactoringRule {

	private String nameViolatedVariables[];
	private String fileToBeRefactored;
	private List<Statement> nodeLists = new ArrayList<Statement>();

	@Override
	public String getDescription() {
		return "Refactor files that have restriction on a cloud with equivalent Collections APIs.";
	}

	@Override
	public String getName() {
		return "Adapt FileOutputStream to GAE";
	}

	@Override
	public String getAbstractRestrictedClassName() {
		return "CRSFileOutputStream";
	}

	@Override
	public String getCommunicationClassName() {
		return "CRS4GAEFileOutputStream";
	}

	@Override
	public String getRestrictedClass() {
		return "java.io.FileOutputStream";
	}

	public String getSuperRestrictedClass() {
		return "java.io.OutputStream";
	}

	public List<Statement> getNodeLists() {
		return nodeLists;
	}

	@Override
	public boolean visit(ClassInstanceCreation node) {

		final ITypeBinding typeBinding = node.getType().resolveBinding();
		// Get the arguments and turn them into expression.
		final List<Expression> arguments = ASTHelper.arguments(node);

		// Tree-nodes creators (ASTBuilder) and refactorings (Refactorings)
		ASTBuilder builder = this.ctx.getASTBuilder();
		Refactorings refactorings = this.ctx.getRefactorings();

		// Instantiated classes that have only one parameter. Ex: new
		// File(parameter)
		if (typeBinding != null) {
			// Full class name, equivalent to import
			final String fullNodeClassName = typeBinding.getQualifiedName();
			if (this.getRestrictedClass().equals(fullNodeClassName)) {
				// Replaces the old node with the new one from ASTBuilder
				if (arguments.size() == 1) {
					refactorings.replace(node,
							builder.createNewClass(this.getCommunicationClassName(), builder.copy(arguments.get(0))));
				} else if (arguments.size() == 2) {
					refactorings.replace(node, builder.createNewClass(this.getCommunicationClassName(),
							builder.copy(arguments.get(0)), builder.copy(arguments.get(1))));
				}
			}
		}
		// Return Tree
		return VISIT_SUBTREE;
	}

	@Override
	public boolean visit(FieldDeclaration node) {
		// Tree-nodes creators (ASTBuilder) and refactorings (Refactorings)
		ASTBuilder builder = this.ctx.getASTBuilder();
		Refactorings refactorings = this.ctx.getRefactorings();

		final ITypeBinding typeBinding = node.getType().resolveBinding();
		if (typeBinding != null) {
			String fullNodeClassName = typeBinding.getQualifiedName();
			if (this.getRestrictedClass().equals(fullNodeClassName)) {
				refactorings.replace(node.getType(), builder.newSimpleType(this.getAbstractRestrictedClassName()));
			}
		}
		return VISIT_SUBTREE;
	}

	public boolean visit(VariableDeclarationStatement node) {
		// Tree-nodes creators (ASTBuilder) and refactorings (Refactorings)
		ASTBuilder builder = this.ctx.getASTBuilder();
		Refactorings refactorings = this.ctx.getRefactorings();

		final ITypeBinding typeBinding = node.getType().resolveBinding();
		if (typeBinding != null) {
			String fullNodeClassName = typeBinding.getQualifiedName();
			if (this.getRestrictedClass().equals(fullNodeClassName)) {
				refactorings.replace(node.getType(), builder.newSimpleType(this.getAbstractRestrictedClassName()));
			}
		}
		return VISIT_SUBTREE;
	}

	public boolean visit(MethodDeclaration node) {

		if (node.getReturnType2() != null) {
			// Tree-nodes creators (ASTBuilder) and refactorings (Refactorings)
			ASTBuilder builder = this.ctx.getASTBuilder();
			Refactorings refactorings = this.ctx.getRefactorings();

			final ITypeBinding typeBinding = node.getReturnType2().resolveBinding();
			if (typeBinding != null) {
				String fullNodeClassName = typeBinding.getQualifiedName();
				if (this.getRestrictedClass().equals(fullNodeClassName)) {
					refactorings.replace(node.getReturnType2(),
							builder.newSimpleType(this.getAbstractRestrictedClassName()));
				}
			}
		}
		return VISIT_SUBTREE;
	}

	public boolean checkViolatedVariables(String name) {
		for (String nameCurrent : nameViolatedVariables) {
			if (name.equals(nameCurrent))
				return true;
		}
		return false;
	}

	@Override
	public boolean visit(ExpressionStatement node) {

		ASTBuilder builder = this.ctx.getASTBuilder();
		Refactorings refactorings = this.ctx.getRefactorings();
		final MethodInvocation mi = ASTHelper.asExpression(node, MethodInvocation.class);
		if (mi != null && mi.getExpression() != null) {
			String variableName = mi.getExpression().toString();

			this.fileToBeRefactored = ASTHelper.getFileName(node);
			nameViolatedVariables = readJSONFromIdentificationEngine();

			// if (this.checkViolatedVariables(variableName)) {
			if (ASTHelper.isMethod(mi, this.getRestrictedClass(), "write", "byte[]", "int", "int")
					|| ASTHelper.isMethod(mi, this.getSuperRestrictedClass(), "write", "byte[]", "int", "int")) {
				if (mi.getParent().getParent().getParent().getNodeType() == ASTNode.WHILE_STATEMENT) {
					Expression argument1 = ASTHelper.arguments(mi).get(0);
					Expression argument3 = ASTHelper.arguments(mi).get(2);
					WhileStatement whi = (WhileStatement) mi.getParent().getParent().getParent();
					updateArgumentOfStatements(argument1, getAllStatementsBeforeNode(node), getParameterWhile(whi));
					removeArgumentOfStatements(argument3, getAllStatementsBeforeNode(node));
					refactorings.replace(whi,
							builder.toStmt(builder.invoke(variableName, "write", builder.copy(argument1))));
				}
			}
			// }
		}

		return true;
	}

	public Expression getParameterWhile(WhileStatement node) {
		if (node.getExpression().getNodeType() == ASTNode.INFIX_EXPRESSION) {
			InfixExpression e = (InfixExpression) node.getExpression();
			Assignment a = (Assignment) ASTHelper.removeParentheses(e.getLeftOperand());
			if (a.getRightHandSide().getNodeType() == ASTNode.METHOD_INVOCATION) {
				MethodInvocation mi = (MethodInvocation) a.getRightHandSide();
				return mi.getExpression();
			}
		}
		return null;
	}

	private void updateArgumentOfStatements(Expression argument, List<Statement> allStatementsBeforeNode,
			Expression name) {

		// Tree-nodes creators (ASTBuilder) and refactorings (Refactorings)
		ASTBuilder builder = this.ctx.getASTBuilder();
		Refactorings refactorings = this.ctx.getRefactorings();

		for (Statement statement : allStatementsBeforeNode) {
			if (statement instanceof VariableDeclarationStatement) {
				VariableDeclarationStatement var = ASTHelper.as(statement, VariableDeclarationStatement.class);
				final VariableDeclarationFragment vdf = ASTHelper.fragments(var).get(0);
				if (ASTHelper.isSameLocalVariable(argument, vdf.resolveBinding())) {
					refactorings.replace(vdf.getInitializer(), builder.createNewMethodStatic(
							super.getClassNameCRSUtils(), "convertFileToByteArray", builder.copy(name)));
				}
			}
		}

	}

	private void removeArgumentOfStatements(Expression argument, List<Statement> allStatementsBeforeNode) {
		for (Statement statement : allStatementsBeforeNode) {
			if (statement instanceof VariableDeclarationStatement) {
				VariableDeclarationStatement var = ASTHelper.as(statement, VariableDeclarationStatement.class);
				final VariableDeclarationFragment vdf = ASTHelper.fragments(var).get(0);
				if (ASTHelper.isSameLocalVariable(argument, vdf.resolveBinding())) {
					this.ctx.getRefactorings().remove(var);
				}
			}
		}
	}

	public List<Statement> getAllStatementsBeforeNode(Statement node) {

		if (ASTHelper.getPreviousStatement(node) != null) {
			nodeLists.add(ASTHelper.getPreviousStatement(node));
			getAllStatementsBeforeNode(ASTHelper.getPreviousStatement(node));
		}
		return getNodeLists();
	}

	@Override
	public String[] readJSONFromIdentificationEngine() {
		JSONParser parser = new JSONParser();
		String[] arrayString = null;
		try {
			String currentFile = System.getProperty("user.home") + "/crsanalyzer/"
					+ fileToBeRefactored.replace(".java", ".json");
			FileReader fileReader = new FileReader(currentFile);
			if (fileReader != null) {
				Object obj = parser.parse(fileReader);
				JSONObject json = (JSONObject) obj;
				JSONAnalyzer jAux = new JSONAnalyzer();
				JSONArray arrayValues = jAux.getValuesOfArray(json, "variableDeclaration", "variable",
						this.getRestrictedClass());
				if (arrayValues != null) {
					arrayString = jAux.jsonArrayToString(arrayValues);
				}
			}
		} catch (

		FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return arrayString;
	}

}